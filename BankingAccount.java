public class BankingAccount {

    public int balance;

    public void deposit(int sum) {
        balance = balance + sum;
    }

    public void withdraw(int sum) {
        balance = balance - sum;
    }
}
