public class Homework8 {

    public static void main(String[] args) {

        BankingAccount myAccount = new BankingAccount();
        myAccount.deposit(500);
        System.out.println("Deposit of " + myAccount.balance + " dollars");
        System.out.println(myAccount.balance);

        myAccount.withdraw(40);
        System.out.println("Withdrawal of " + 40 + " dollars");
        if(myAccount.balance == 460) {
            System.out.println("My correct balance is 460 dollars");
        } else {
            System.out.println("My balance is not correct");
        }
        System.out.println(myAccount.balance);

        myAccount.withdraw(120);
        System.out.println("Withdrawal of " + 120 + " dollars");
        if(myAccount.balance == 340) {
            System.out.println("My correct balance is 340 dollars");
        }else{
            System.out.println("My balance is not correct");
        }
        System.out.println(myAccount.balance);

        myAccount.deposit(150);
        System.out.println("Deposit of " + 150 + " dollars");
        if(myAccount.balance == 490) {
            System.out.println("My correct balance is 490 dollars");
        }else{
            System.out.println("My balance is not correct");
        }
        System.out.println(myAccount.balance);

        myAccount.deposit(35);
        System.out.println("Deposit of " + 35 + " dollars");
        if(myAccount.balance == 525) {
            System.out.println("My correct balance is 525 dollars");
        }else{
            System.out.println("My balance is not correct");
        }
        System.out.println(myAccount.balance);
    }
}

